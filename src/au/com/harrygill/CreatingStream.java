package au.com.harrygill;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class CreatingStream {

    public static void main(String[] args) {

        Stream<String> s1 = Stream.of();
        Stream<Integer> s2 = Stream.of(5, 4, 3, 2, 1);

        Stream<Integer> emptyStream = Stream.empty();

        List<String> listOfStrings = new ArrayList<>();
        listOfStrings.add("Hello");
        listOfStrings.add("World");

        Stream<String> streamOfStrings = listOfStrings.stream();

        Stream<Double> infiniteRandomNumbers = Stream.generate(Math::random);

        Stream<Integer> infiniteOddNumbers = Stream.iterate(1, n -> n + 2);

        //finite stream using iterate
        Stream<Integer> oddNumbersLessThanHundred = Stream.iterate(1, n -> n < 100, n -> n + 2);

    }
}